#include "neuron.hpp"

/******************************************************************************/

// A helper function to compute the amount of time that has elapsed since the previous action potential
double ElapsedTime(std::chrono::high_resolution_clock::time_point previousEventTime)
{
	std::chrono::high_resolution_clock::time_point currentTime = std::chrono::high_resolution_clock::now();
	auto t = std::chrono::duration_cast<std::chrono::microseconds> (currentTime - previousEventTime).count();
	return t;
}

/******************************************************************************/

Neuron::Neuron(double thresh, double tau, std::string name):
												              THRESHOLD_VOLTAGE(thresh),
																			TIME_CONSTANT(tau),
																			RESTING_POTENTIAL(RP),
																			RECTIFICATION_SLOPE(RS),
																			NAME(name),
																			d_inputMembranePotential(RP),
																			d_membranePotential(RP),
													 						d_state(Rest)
{
	// Take an initial time stamp
	d_previousActionPotentialTime = std::chrono::high_resolution_clock::now();
	d_previousIterationTime = std::chrono::high_resolution_clock::now();
}

/******************************************************************************/

Neuron::~Neuron()
{
	// Free all pointers to axon objects
  for (auto itr = p_axons.begin(); itr != p_axons.end(); ++itr)
  {
    delete itr->second;
  }
}

/******************************************************************************/

void Neuron::ConnectPostSynapticNeuron(Neuron* p_postSynapticNeuron, double synapticStength)
{
	// Push a new axon connecting the neuron to a post synaptic neuron
	p_axons[p_postSynapticNeuron->Name()] = new Axon(p_postSynapticNeuron, synapticStength);
}

/******************************************************************************/

void Neuron::StimulateNeuron(double stimulus)
{
	// Inputs are only accepted when the neuron is in its resting state
	switch(d_state)
	{
		case Rest :
			d_inputMembranePotential += stimulus;
			break;

		default :
			d_inputMembranePotential = RESTING_POTENTIAL;
			break;
	}
}

/******************************************************************************/

void Neuron::ComputeMembranePotential()
{
	// The input potential (post synaptic potential) decays by an amount
	// proportional to the elapsed time since the previous input was applied.
	d_inputMembranePotential = d_inputMembranePotential - FORGETTING_FACTOR;

	// Constrain the input potential to be greater than or equal to zero.
	if (d_inputMembranePotential < 0)
	{
		d_inputMembranePotential = 0;
	}

	// Solve the ODE using the Euler method:	 y_n+1 = y_n + (h +  F(y_n, t_n))
	double h = ElapsedTime(d_previousIterationTime) * 1e-6;
	double F = (d_inputMembranePotential - (d_membranePotential - RESTING_POTENTIAL)) / TIME_CONSTANT;
	d_membranePotential = d_membranePotential + (h * F);

	// Update the timestamp
	d_previousIterationTime = std::chrono::high_resolution_clock::now();
}

/******************************************************************************/

void Neuron::UpdateNeuronState()
{
	// Update the instantaneous membrane potential
	ComputeMembranePotential();

	// Go through the neuron's finite state machine
	switch(d_state)
	{
		case Rest :

			// Wait for an action potential to be fired
			if(d_membranePotential > THRESHOLD_VOLTAGE)
			{
				d_state = ActionPotential;
			}
			break;

		case ActionPotential :

			// Time-stamp the action potential, and enter the refractory period
			d_previousActionPotentialTime = std::chrono::high_resolution_clock::now();
			d_state = RefractoryPeriod;
			break;

		case RefractoryPeriod :

			// After the refractory period has elapsed, enter the resting state
			if(ElapsedTime(d_previousActionPotentialTime) > REFRACTORY_PERIOD)
			{
				d_state = Rest;
			}
			break;
	}

	// Transmit the current membrane potential along all axons of the neuron
	TransmitMembranePotential();
}

/******************************************************************************/

void Neuron::TransmitMembranePotential()
{
	// Iterate through all axons, and compute the transmitted signal for each.
	double signalStrength = 0;
	for(auto iter_axon = p_axons.begin(); iter_axon != p_axons.end(); iter_axon++)
	{
		// Transmit signals to the post-synaptic neurons
		signalStrength = d_membranePotential * (iter_axon->second->d_synapticStrength);
		iter_axon->second->p_postSynapticNeuron->StimulateNeuron(signalStrength);
	}
}


void Neuron::TransmitMembranePotential(std::string postSynapticNeuron)
{
	// Transmit signals to the specified post-synaptic neuron
	double signalStrength = 0;
	signalStrength = d_membranePotential * p_axons[postSynapticNeuron]->d_synapticStrength;
	p_axons[postSynapticNeuron]->p_postSynapticNeuron->StimulateNeuron(signalStrength);
}

/******************************************************************************/

double Neuron::FiringRate()
{
	// Rectification implemented using a sigmoid to constrain the output between [0, SATURATION]
	double firingRate = SATURATION / (1.0 + exp(-RECTIFICATION_SLOPE * (d_membranePotential - THRESHOLD_VOLTAGE)));
	return firingRate;
}

/******************************************************************************/

void Neuron::PrintConnectedNeurons()
{
	std::cout << "Neurons connected to " << NAME<< " are\n";
	for(auto iter_axon = p_axons.begin(); iter_axon != p_axons.end(); iter_axon++)
	{
		// Print the names of all connected neurons
		std::cout<< "\t" << iter_axon->second->p_postSynapticNeuron->Name() << "\n";
	}
}

/******************************************************************************/
