#include "eye.hpp"

// Default constructor.
Eye::Eye() :
						cam(),
						CAMERA_SERIAL_NUMBER(""),
						FOV_HEIGHT(FRAME_HEIGHT),
						FOV_WIDTH(FRAME_WIDTH),
						EYE_ORIENTATION(Left),
						MINIMUM_OBSTACLE_DIAMETER(MIN_DETECTION_SIZE),
						CALIBRATION_DATA_FILE("calibrationData.xml"),
						CAMERA_MATRIX(3, 3, CV_32FC1)
{
	// Load the distortion camera parameters matrix and distortion coefficients matrix that will be used to undistort the fish-eye images.
	cv::FileStorage fs(CALIBRATION_DATA_FILE.c_str(), cv::FileStorage::READ);
	fs["intrinsic"] >> CAMERA_MATRIX;
	fs["distCoeffs"] >> DISTORTION_COEFFICIENTS;
	fs.release();

	// Set up the parameters for the blob detector
	d_blobDetectorParams.filterByColor = true;
	d_blobDetectorParams.blobColor = 255;		// Detect white objects as blobs (default is to detect darker objects)

	d_blobDetectorParams.filterByCircularity = true;
	d_blobDetectorParams.minCircularity = 0.8;

	d_blobDetectorParams.filterByConvexity = true;
	d_blobDetectorParams.minConvexity = 0.8;

	// Filter by Area.
	d_blobDetectorParams.filterByArea = true;
	d_blobDetectorParams.minArea = MINIMUM_OBSTACLE_DIAMETER*M_PI;

	d_blobDetectorParams.filterByInertia = false;

	// Create the blob detector
	d_blobDetector = cv::SimpleBlobDetector::create(d_blobDetectorParams);
}

/******************************************************************************/

// Custom constructor
Eye::Eye (Orientation s, std::string serialNumber) :
																						cam(),
																						CAMERA_SERIAL_NUMBER(serialNumber),
																						FOV_HEIGHT(FRAME_HEIGHT),
																						FOV_WIDTH(FRAME_WIDTH),
																						EYE_ORIENTATION(s),
																						MINIMUM_OBSTACLE_DIAMETER(MIN_DETECTION_SIZE),
																						CALIBRATION_DATA_FILE("calibrationData.xml"),
																						CAMERA_MATRIX(3, 3, CV_32FC1)
{
	// Load the distortion camera parameters matrix and distortion coefficients matrix that will be used to undistort the fish-eye images.
  cv::FileStorage fs(CALIBRATION_DATA_FILE.c_str(), cv::FileStorage::READ);
  fs["intrinsic"] >> CAMERA_MATRIX;
  fs["distCoeffs"] >> DISTORTION_COEFFICIENTS;
  fs.release();

  // Set up the parameters for the blob detector
  d_blobDetectorParams.filterByColor = true;
  d_blobDetectorParams.blobColor = 255;		// Detect white objects as blobs (default is to detect darker objects)

  d_blobDetectorParams.filterByCircularity = true;
  d_blobDetectorParams.minCircularity = 0.8;

  d_blobDetectorParams.filterByConvexity = true;
  d_blobDetectorParams.minConvexity = 0.8;

  // Filter by Area.
  d_blobDetectorParams.filterByArea = true;
  d_blobDetectorParams.minArea = MINIMUM_OBSTACLE_DIAMETER * M_PI;

  d_blobDetectorParams.filterByInertia = false;

  // Create the blob detector
  d_blobDetector = cv::SimpleBlobDetector::create(d_blobDetectorParams);
}

/******************************************************************************/

/*
	Enumerated type values for setting the camera parameters can be obtained here:
			https://github.com/jaybo/OpenCVGraph/blob/master/OpenCVGraph/Capture/CameraSDKs/Ximea/API/xiApi.h
*/

void Eye::OpenEye()
{
	// Open the eye's camera
	if (CAMERA_SERIAL_NUMBER.empty())
	{
		// Retrieving a handle to the first camera device available
		cam.OpenFirst();
	}
	else
	{
		// Open the camera according to its serial number.
		std::vector<char> SN(CAMERA_SERIAL_NUMBER.c_str(), CAMERA_SERIAL_NUMBER.c_str() + CAMERA_SERIAL_NUMBER.size() + 1u);
		cam.OpenBySN(&SN[0]);
	}

	// Maximize the camera's bandwidth limit
	cam.SetBandwidthLimit(cam.GetBandwidthLimit_Maximum());

	// Set the data Format
	cam.SetImageDataFormat(XI_RGB24);

	// Set downsampling
	cam.SetDownsampling(XI_DWN_4x4);

	// Set downsampling type
	cam.SetDownsamplingType(XI_BINNING);
//	cam.SetDownsamplingType(XI_SKIPPING);

	// Minimize the sensor and image bit depth to maximize frame rate.
	cam.SetSensorDataBitDepth(cam.GetSensorDataBitDepth_Minimum());
	cam.SetImageDataBitDepth(cam.GetImageDataBitDepth_Minimum());

	// Set the image dimensions & offsets
	cam.SetWidth(FOV_WIDTH);
	cam.SetHeight(FOV_HEIGHT);
	cam.SetOffsetX(OFFSET_X);
	cam.SetOffsetY(OFFSET_Y);

	// Set exposure... 10000 = 10 ms
	//cam.SetExposureTime(10166);
	cam.SetExposureTime(1000);

	// Set the camera gain
	cam.SetGain(15);

	// Set white balance to on.
	cam.EnableWhiteBalanceAuto();

	// Set acquisition timing mode
	cam.SetAcquisitionTimingMode(XI_ACQ_TIMING_MODE_FRAME_RATE);

	// Set the frame rate
	cam.SetFrameRate(31);

	// Print a set of relevant camera parameters following the camera initialization
	std::cout << std::endl;
	std::cout << "\t~~~~ Camera Parameters ~~~~ "<<std::endl;
	std::cout << "Image Resolution:\t"<<cam.GetWidth() << " x " << cam.GetHeight() << std::endl;
	std::cout << "Frame Rate:\t"<<cam.GetFrameRate() <<std::endl;
	std::cout << "Sensor Clock Frequency (Hz):\t" << cam.GetSensorClockFrequencyHz() << std::endl;
	std::cout << "Sensor Data Bit Depth (bits/pixel):\t" << cam.GetSensorDataBitDepth() << std::endl;
	std::cout << "Image Data Bit Depth (bits/pixel):\t" << cam.GetImageDataBitDepth() << std::endl;
	std::cout << "Bandwidth Limit (MB/s):\t" << cam.GetBandwidthLimit() << std::endl;
	std::cout << std::endl;

	// Start acquiring camera images
	cam.StartAcquisition();
}

/******************************************************************************/

// A helper function to rotate images by a specified angle. Rotation is done in place on the image.
void Rotate(cv::Mat src, double angle)
{
    cv::Point2f pt(src.cols/2., src.rows/2.);

		// Determine the transformation matrix needed to rotate the image
    cv::Mat transformationMatrix = cv::getRotationMatrix2D(pt, angle, 1.0);

		// Rotate the image using the transformation matrix
    cv::warpAffine(src, src, transformationMatrix, cv::Size(src.cols, src.rows));
}


void Eye::GetNewImage ()
{
	// Obtain a new raw image
	d_currentRawFrame = cam.GetNextImageOcvMat();

	// Rotate the image obtained by the camera to align the two images.
	switch (EYE_ORIENTATION)
	{
		case Left :
			Rotate(d_currentRawFrame, -90);																						// Left eye must rotate by -90°
			break;

		case Right :
			Rotate(d_currentRawFrame, 90);																						// Right eye must rotate by +90°
			break;
	}

	// Remove image distortion arising from the properties of the fish-eye lense
	//cv::undistort(d_currentRawFrame, d_currentUndistortedFrame, CAMERA_MATRIX, DISTORTION_COEFFICIENTS);
	d_currentUndistortedFrame = d_currentRawFrame;
}

/******************************************************************************/

void Eye::CloseEye ()
{
	// Stop acquiring camera images
  cam.StopAcquisition();

	// Close the camera
  cam.Close();
}

/******************************************************************************/


void Eye::DetectObjects (bool plot)
{
  // Processing each color will generate a mask
  cv::Mat Mask;
	cv::Mat Mask2;

  // We will process the image for colors in the hsv space, not the bgr space
  cv::cvtColor(d_currentUndistortedFrame, d_hsvImage, cv::COLOR_BGR2HSV);

	// Equalize the histogram of the saturation and lightness chanels to improve performance
	std::vector<cv::Mat> channels;
	cv::split(d_hsvImage, channels);
	cv::equalizeHist(channels[1], channels[1]);
	cv::merge(channels,d_hsvImage);

	// Good reference for how to find HSV values of different colors
	// https://docs.opencv.org/3.4/df/d9d/tutorial_py_colorspaces.html


	cv::inRange(d_hsvImage, cv::Scalar(0, 100, 100), cv::Scalar(10, 255, 255), Mask);
	cv::inRange(d_hsvImage, cv::Scalar(170, 100, 100), cv::Scalar(180, 255, 255), Mask2);
/*	cv::inRange(d_hsvImage, cv::Scalar(0, 100, 100), cv::Scalar(10, 255, 255), Mask);
	cv::inRange(d_hsvImage, cv::Scalar(170, 100, 100), cv::Scalar(180, 255, 255), Mask2); */

	Mask = Mask | Mask2;

  // Detect the red blobs. Extract their coordinates and size.
  d_blobDetector->detect(Mask, d_redObjectKeypoints);

  // If the plot flag is set, plot a circle about the object. For debugging.
  if (plot) DrawOutline(Mask, d_redObjectKeypoints);

  // Consider only the blue pixels.
	cv::inRange(d_hsvImage, cv::Scalar(95, 100, 100), cv::Scalar(112 , 255, 255), Mask);
	//cv::inRange(d_hsvImage, cv::Scalar(90, 100, 100), cv::Scalar(170, 255, 255), Mask);

	// Detect the blue blobs. Extract their coordinates and size.
  d_blobDetector->detect(Mask, d_blueObjectKeypoints);

  // If the plot flag is set, plot a circle about the object. For debugging.
  if (plot) DrawOutline(Mask, d_blueObjectKeypoints);

  // Consider only the green pixels.
	cv::inRange(d_hsvImage, cv::Scalar(20, 100, 100), cv::Scalar(90, 255, 255), Mask);
	//cv::inRange(d_hsvImage, cv::Scalar(20, 100, 100), cv::Scalar(90, 255, 255), Mask);

	// Detect the green blobs. Extract their coordinates and size.
  d_blobDetector->detect(Mask, d_greenObjectKeypoints);

  // If the plot flag is set, plot a circle about the object. For debugging.
  if (plot) DrawOutline(Mask, d_greenObjectKeypoints);
}

/******************************************************************************/

// Compute the stimulation strength and neuron index
void computeStimulation(std::vector<cv::KeyPoint> keypointsVector, std::vector<double> &stimVector, Orientation eyeOrientation, int FOV_WIDTH, double MAXIMUM_OBSTACLE_DIAMETER)
{
	// Variables to compute the signal strength generated by an obstacle, and index of the corresponding neuron to be stimulated.
	double signalStrength = 0;
	unsigned int neuronIndex = 0;
	unsigned int offset = 0;

	// Based on the location and size of the objects, stimulate the proper input neuron(s).
	for (unsigned int i = 0; i < keypointsVector.size(); i++)
	{
		// Determine the raw signal strength
		signalStrength = keypointsVector[i].size;

		// Pass the raw signal through a sigmoid filter to limit its value between 0 and 1
		//signalStrength = 2 / (1 + (exp(-0.05 * signalStrength))) - 1;
		signalStrength = 1 / (1 + (exp(-0.05 * signalStrength)));

		// The offset of the index depends on whether the object is seen with the right or left eye.
		switch(eyeOrientation)
		{
			case Left :

				// The object is seen with the left eye. The neuron index starts from 0
				offset = 0;
				break;

			case Right :

				// The object is seen with the right eye. The neuron index starts from the midpoint of the input layer
				offset = NETWORK_SIZE / 2;
				break;
		}

		// Determine the neuron index.
		neuronIndex = offset + (((keypointsVector[i].pt.x) / FOV_WIDTH) * (NETWORK_SIZE / 2));

		// Update the input neuron stimulation vector
		stimVector[neuronIndex] = signalStrength;
	}
}

/******************************************************************************/

// A helper function to reset the values of vectors to zero between a specified lower index and upper index
void resetStimVectors(int lowerBound, int upperBound, std::vector<double> &redStimVector, std::vector<double> &blueStimVector, std::vector<double> &greenStimVector)
{
	for (int i = lowerBound; i < upperBound; i++)
	{
		redStimVector[i] = 0;
		blueStimVector[i] = 0;
		greenStimVector[i] = 0;
	}
}

void Eye::UpdateStimVectors(std::vector<double> &redStimVector, std::vector<double> &blueStimVector, std::vector<double> &greenStimVector)
{
	// Reset the values in the stimulation vector of the current eye to 0. This makes room for the new stimulation values to be computed.
	switch (EYE_ORIENTATION)
	{
		case Left :
			resetStimVectors(0, NETWORK_SIZE/2, redStimVector, blueStimVector, greenStimVector);
			break;

		case Right :
			resetStimVectors(NETWORK_SIZE/2, NETWORK_SIZE, redStimVector, blueStimVector, greenStimVector);
			break;
	}

	// Based on the location and size of the objects seen, stimulate the red input neurons.
	computeStimulation(d_redObjectKeypoints, redStimVector, EYE_ORIENTATION, FOV_WIDTH, MAXIMUM_OBSTACLE_DIAMETER);

	// Based on the location and size of the objects seen, stimulate the blue input neurons.
	computeStimulation(d_blueObjectKeypoints, blueStimVector, EYE_ORIENTATION, FOV_WIDTH, MAXIMUM_OBSTACLE_DIAMETER);

	// Based on the location and size of the objects seen, stimulate the green input neurons.
	computeStimulation(d_greenObjectKeypoints, greenStimVector, EYE_ORIENTATION, FOV_WIDTH, MAXIMUM_OBSTACLE_DIAMETER);
}

/******************************************************************************/

cv::Mat Eye::CurrentFrame()
{
	return d_currentUndistortedFrame;
}


/******************************************************************************/

void Eye::DrawOutline (cv::Mat mask, std::vector<cv::KeyPoint> keypoints)
{
  // Draw detected blobs as circles. DrawMatchesFlags::DRAW_RICH_objectKeypoints flag ensures the size of the circle corresponds to the size of blob
  cv::drawKeypoints(d_currentUndistortedFrame, keypoints, d_currentUndistortedFrame, cv::Scalar(255,255,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

	cv::imshow("Camera " + CAMERA_SERIAL_NUMBER + " View", d_currentUndistortedFrame);
  //cv::imshow("Camera " + CAMERA_SERIAL_NUMBER + " Objects Mask", mask);
  cv::waitKey(1);
}
