#ifndef LEAKYNEURON_H
#define LEAKYNEURON_H

#include <cmath>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <stdint.h>
#include "parameters.h"
#include <chrono>
#include <numeric>
#include <deque>
#include <map>

class Neuron;


/***************************************************************************
*  @brief: A structure representing a neuron axon.
*
*  @details: Every axon contains two key units of information:
*             - The post synaptic neuron it connects to
*             - The synaptic strength between the two connected neurons
***************************************************************************/
struct Axon
{
  // An axon connects to a post synaptic neuron
  Neuron* p_postSynapticNeuron;

  // An axon carries a post synaptic potential to the post synaptic neuron
  double d_synapticStrength;

  // Constructor for the Axon
  Axon(Neuron* PSN, double SS): p_postSynapticNeuron(PSN), d_synapticStrength(SS) {}
};



/***************************************************************************
*  @brief: An enumerated type to keep track of the neuron's state.
*
*  @details: The neuron can be in one of three states:
*             - Rest
*             - Action Potential
*             - Refractory Period
***************************************************************************/
enum NeuronState
{
  Rest,
  ActionPotential,
  RefractoryPeriod
};


class Neuron {

private:

  /***************************************************************************
  *  @brief: The neuron's constant parameters
  *
  *  @details: The neuron parameters are:
  *             - Threshold voltage (triggers action potential if exceeded)
  *             - Time constant (affects charge/discharge rate)
  *             - Resting potential (membrane potential under no stimulation)
  *             - Rectification slope (determines the shape of the sigmoid used for firing-rate estimation)
  *             - Name (a unique identifier for accessing the neuron object in a hash table)
  ***************************************************************************/
  const double THRESHOLD_VOLTAGE;
	double TIME_CONSTANT;
  double RESTING_POTENTIAL;
  double RECTIFICATION_SLOPE;
  std::string NAME;


  /***************************************************************************
  *  @brief: The neuron's instantaneous input voltage
  ***************************************************************************/
  double d_inputMembranePotential;


  /***************************************************************************
  *  @brief: The neuron's instantaneous membrane voltage. This is the
  *          quantity used to trigger action potentials.
  ***************************************************************************/
  double d_membranePotential;


  /***************************************************************************
  *  @brief: The neuron's current state.
  *
  *  @details: The neuron can be in one of three states:
  *             - Rest
  *             - Action Potential
  *             - Refractory Period
  ***************************************************************************/
  NeuronState d_state;


  /***************************************************************************
  *  @brief: Time stamps used to keep track of neuron events
  *
  *  @details: There are two time stamps that each neuron uses:
  *             - The previous time an action potential was fired
  *             - The computation time (this is used to compute the time
  *               step for each iteration of solving the ODE)
  ***************************************************************************/
  std::chrono::high_resolution_clock::time_point d_previousActionPotentialTime;
  std::chrono::high_resolution_clock::time_point d_previousIterationTime;


  /***************************************************************************
  *  @brief: A hash map holding pointers to all of the neuron's axons.
  *
  *  @details: This dictionary allows us to access an axon using its
  *            post synaptic neuron as a key.
  *
  *             - Key...... Neuron name (std::string)
  *             - Value.... Pointer to Axon (Axon*)
  ***************************************************************************/
  std::map<std::string, Axon*> p_axons;

public:


  /***************************************************************************
  *  @brief: Custom constructor
  *
  *  @details: Method to create a new neuron object with a specified
  *            voltage threshold, time constant, and name identifier.
  *
  *  @params[in]: double
  *                 - The neuron's threshold voltage
  *
  *               double
  *                 - The neuron's time constant
  *
  *               std::string
  *                 - The neuron name
  ***************************************************************************/
	Neuron(double thresh, double tau, std::string name);


  /***************************************************************************
  *  @brief: Custom destructor.
  *
  *  @details: Clean up all pointers to axons created for the neuron.
  ***************************************************************************/
  ~Neuron();


  /***************************************************************************
  *  @brief: A function to connect a neuron to the current neuron.
  *
  *  @details: A new axon is created linking the two neurons. The strength
  *            of the connection between the neurons is specified when calling
  *            this method.
  *
  *  @params[in]: Neuron*
  *                 - A pointer to the post synaptic neuron
  *
  *               double
  *                 - The value of the synaptic strength between the two neurons
  ***************************************************************************/
  void ConnectPostSynapticNeuron(Neuron* p_postSynapticNeuron, double synapticStength);


  /***************************************************************************
  *  @brief: A function to stimulate the current neuron
  *
  *  @details: This method accumulates the input membrane potential by an
  *            amount equal to the stimulus.
  *
  *            Inputs can only be effective when the neuron is in the
  *            resting state. Otherwise, the input remains at the resting
  *            potential for the duration of the refractory period.
  *
  *  @params[in]: double
  *                 - The value of stimulus to add.
  ***************************************************************************/
	void StimulateNeuron(double stimulus);


  /***************************************************************************
  *  @brief: A method to compute the neuron's membrane potential
  *
  *  @details: This method imlements a leaky-integrate-and-fire neuron
  *            model. In order to solve the first order ODE, the Euler
  *            method is used.
  *
  *            The input membrane potential is used to drive the membrane
  *            potential. Over time, the input membrane potential decays
  *            to zero if an action potential is not reached.
  ***************************************************************************/
  void ComputeMembranePotential();


  /***************************************************************************
  *  @brief: A high-level method to implement the neuron's behavior
  *
  *          keep track of the neuron's state, compute
  *          the membrane potential in real time, and then transmit
  *          signals to post-synaptic neurons.
  *
  *  @details: This method keeps track of the neuron's state, computes
  *            the membrane potential in real time, and then transmits,
  *            signals to post-synaptic neurons.

  *            This method also uses a Finite State Machine to keep track of
  *            the neuron's states:
  *             - Resting
  *             - Action Potential
  *             - Refractory Period
  ***************************************************************************/
	void UpdateNeuronState();


  /***************************************************************************
  *  @brief: A function to stimulate connected post-synaptic neurons.
  *
  *  @details: In order to replicate biological neurons, signals are
  *            constantly being transmitted to the post-synpatic neurons.
  *            However, the value transmitted is non-zero only when
  *            the membrane potential exceeds the threshold voltage.
  *
  *            This method has been overloaded to allow transmitting to
  *            a single specified neuron.
  *
  *            If a target neuron is not passed, then all connected neurons
  *            are stimulated.
  ***************************************************************************/
	void TransmitMembranePotential();
  void TransmitMembranePotential(std::string postSynapticNeuron);


  /***************************************************************************
  *  @brief: A function to compute the neuron's firing rate.
  *
  *  @details: Instantaneous frequency is estimated using a transformation
  *            from input membrane potential to firing rate. This is done
  *            using a sigmoid function.
  ***************************************************************************/
  double FiringRate();


  /***************************************************************************
  *  @brief: Getter functions
  *
  *  @details: Methods to retrieve member variables of the neuron class
  ***************************************************************************/
  void PrintConnectedNeurons();
  double MembranePotential() { return d_membranePotential; }
  double InputMembranePotential() { return d_inputMembranePotential; }
	double FiringThreshold() { return THRESHOLD_VOLTAGE; }
  double TimeConstant() { return TIME_CONSTANT; }
  std::string Name(){ return NAME;}


  /***************************************************************************
  *  @brief: Setter functions
  *
  *  @details: Methods to modify member variables of the neuron class
  ***************************************************************************/
  void SetInputMembranePotential(double stimulus) { d_inputMembranePotential = stimulus; }
  void SetTimeConstant(double tau){ TIME_CONSTANT = tau; }
};

#endif
