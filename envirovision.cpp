#include <arpa/inet.h>
#include <chrono>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <mutex>
#include <time.h>
#include <thread>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include "envirobot.hpp"


//#include "cscore.h"																														// Include on NanoPi

/******SCRIPT MACROS*******/
#define IMU 0           // Enable IMU (if CAMERA == 0, main loop will just print RPY values)
#define DEBUG 0         // Enable Debug Messages
#define IMU_CONFIG 0    // Enable IMU configuring (only needs to be run once / or over MT Manager instead)
#define PRINT_FPS 1     // Enable printing of FPS


/************************/
/****** MAIN METHOD *****/
/************************/

int main(){

	// FPS clock
	//double elapsedTime;
	std::chrono::high_resolution_clock::time_point referenceTime = std::chrono::high_resolution_clock::now();
	std::chrono::high_resolution_clock::time_point currentTime;
	int frameCount = 0;

	// The program will stop when the escape key is pressed.
	uint8_t key;

	// Specify the information for each connected camera.
	// It is necessary to specify the orientation of each camera to be sure
	// that the right and left eyes of the robot aren't flipped.
	std::vector<std::pair<Orientation, std::string>> cameraInfo
	{
  	{Left, "25794959"},
		{Right, "25795659"}
	};

	// Instantiate the Envirobot
	//Envirobot envirobot;																												// Default constructor (useful for only 1 cam)
	Envirobot envirobot(cameraInfo);																							// Custrom constructor

	// Create the connections between all neurons in the envirobot.
	envirobot.CreateConnections();

	// Open the left eye
	envirobot.OpenEyes();

	for(;;)
	{
		// Capture an image with the left eye.
		envirobot.Cycle();

#if PRINT_FPS

		// Measure the FPS realized. Get the current time stamp.
		currentTime = std::chrono::high_resolution_clock::now();

		// Compute time difference between reference time stamp and current time stamp.
		auto duration = std::chrono::duration_cast<std::chrono::microseconds> (currentTime - referenceTime).count();

		// If a second has passed, print the number of frames processed and reset the counter.
		if (duration > 1e6)
		{
			std::cout << "FPS: " << frameCount << std::endl;
			frameCount = 0;
			referenceTime = std::chrono::high_resolution_clock::now();
		}

		// Increment frame counter.
		frameCount++;

#endif


		// If the escape key is pressed, quit the loop.
		key = cv::waitKey(1);
		if(key==27)
		{
			// Stop acquiring camera images, and close the instance of the camera.
			envirobot.CloseEyes();
			return 0;
		}
	}

	// Stop acquiring camera images, and close the instance of the camera.
	envirobot.CloseEyes();
	return 0;
}
