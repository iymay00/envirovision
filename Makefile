PROGRAM = envirovision

SOURCES = envirovision.cpp
SOURCES+= envirobot.cpp
SOURCES+= xiApiPlusOcv.cpp
SOURCES+= eye.cpp
SOURCES+= visuomotorCenter.cpp
SOURCES+= basalGanglia.cpp
SOURCES+= brainstem.cpp
SOURCES+= neuron.cpp
SOURCES+= functionalGroup.cpp

OBJECTS = $(SOURCES:.cpp=.o)

CURRENT_DIR = $(shell pwd)

LIB_DIRS = -L/usr/local/lib

CC = g++ -std=gnu++11

all: $(PROGRAM)

$(PROGRAM): xiApiPlusOcv.o neuron.o functionalGroup.o brainstem.o eye.o envirobot.o basalGanglia.o envirovision.o visuomotorCenter.o
	$(CC) $(OBJECTS) -o $(PROGRAM) -lpthread `pkg-config opencv --cflags --libs` -lm3api -lopencv_core -lopencv_highgui -lopencv_imgproc $(LIB_DIRS) #-lwiringPi -lcscore -lwpiutil -lntcore

xiApiPlusOcv.o: xiApiPlusOcv.cpp
	$(CC) -c xiApiPlusOcv.cpp -g3 -Wall -fmessage-length=0

neuron.o: neuron.cpp
	$(CC) -c neuron.cpp -g3 -Wall -fmessage-length=0

functionalGroup.o: functionalGroup.cpp
	$(CC) -c functionalGroup.cpp -g3 -Wall -fmessage-length=0

brainstem.o: brainstem.cpp
	$(CC) -c brainstem.cpp -I -g3 -Wall -fmessage-length=0

basalGanglia.o: visuomotorCenter.cpp
	$(CC) -c basalGanglia.cpp -I -g3 -Wall -fmessage-length=0

visuomotorCenter.o: visuomotorCenter.cpp
	$(CC) -c visuomotorCenter.cpp -g3 -Wall -fmessage-length=0

eye.o: eye.cpp
	$(CC) -c eye.cpp -g3 -Wall -fmessage-length=0

envirobot.o: envirobot.cpp
	$(CC) -c envirobot.cpp -g3 -Wall -fmessage-length=0

envirovision.o: envirovision.cpp
	$(CC) -c envirovision.cpp -g3 -Wall -fmessage-length=0



clean:
	rm -f *.o *~ $(PROGRAM) $(OBJECTS)
