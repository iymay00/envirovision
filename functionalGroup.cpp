#include "functionalGroup.hpp"

/******************************************************************************/

FunctionalGroup::~FunctionalGroup()
{
  // Free all pointers to neuron objects
  for (auto itr = p_neurons.begin(); itr != p_neurons.end(); ++itr)
  {
    delete itr->second;
  }
}

/******************************************************************************/

void FunctionalGroup::CreateConnections()
{
  //  Iterate through all neurons of the functional group
  for (auto itr = p_neurons.begin(); itr != p_neurons.end(); ++itr)
  {
    // From the hash table, get a key and the neuron pointer that it references
    std::string neuronKey = itr->first;
    Neuron* p_neuron = itr->second;

    // Get the list of all neurons that need to be connected to the current neuron
    std::vector<std::pair <std::string, double>> connectionList = d_connectionMap[neuronKey];

    // Connect post-synaptic neurons to the current neuron
    for(unsigned int i = 0; i < connectionList.size(); i++)
    {
      // Get the post synaptic neuron
      std::string postSynapticNeuronKey = connectionList[i].first;
      Neuron* postSynapticNeuron = p_neurons[postSynapticNeuronKey];

      // Get the synaptic strength
      double synapticStrength = connectionList[i].second;

      // Connect the neuron
      p_neuron->ConnectPostSynapticNeuron(postSynapticNeuron, synapticStrength);
    }
  }
}

/******************************************************************************/
