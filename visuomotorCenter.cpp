#include "visuomotorCenter.hpp"

/******************************************************************************/

VisuomotorCenter::VisuomotorCenter()
{
  // Create the behaviors that the robot can exhibit.
  BEHAVIORS.push_back("green");
  BEHAVIORS.push_back("blue");
  BEHAVIORS.push_back("red");

  // Create the different layers.
  LAYERS.push_back("Input");
  LAYERS.push_back("Response");
  LAYERS.push_back("Auxiliary");

  // Create the neurons of the visuomotor center
  for(unsigned int b = 0; b < BEHAVIORS.size(); b++)
  {
    for(unsigned int l = 0; l < LAYERS.size(); l++)
    {
      for(int n = 0; n < NETWORK_SIZE; n++)
      {
        // Each neuron is given a name based on its behavior, layer, and number
        std::string neuronName = BEHAVIORS[b] + LAYERS[l] + std::to_string(n);

        // Add a neuron with its correct spike threshold and decay rate
        if(LAYERS[l] == "Input")
        {
          p_neurons[neuronName] = new Neuron(INPUT_SPIKE_THRESH, INPUT_TIME_CONSTANT, neuronName);
        }
        else if(LAYERS[l] == "Response")
        {
          p_neurons[neuronName] = new Neuron(RESPONSE_SPIKE_THRESH, RESPONSE_TIME_CONSTANT, neuronName);
        }
        else if(LAYERS[l] == "Auxiliary")
        {
          p_neurons[neuronName] = new Neuron(AUX_SPIKE_THRESH, AUX_TIME_CONSTANT, neuronName);
        }
      }
    }
  }

  // Register the connections between each pre-synaptic neuron and all its post-synaptic neurons
  std::string preSynapticNeuron;
  std::string postSynapticNeuron;

  // Create connections for the visuomotor centers of each behavior (red, blue, and green).
  for(unsigned int b = 0; b < BEHAVIORS.size(); b++)
  {
    // Register the connections between all neurons of the input layer and response layer
    for(int i = 0; i < NETWORK_SIZE / 2; i++)
    {
      //preSynapticNeuron = BEHAVIORS[b] + "Input" + std::to_string(i);
      preSynapticNeuron = BEHAVIORS[b] + "Input" + std::to_string((NETWORK_SIZE / 2) + i);

      // Determine if the connection crosses the midline or not
      if((BEHAVIORS[b] == "red") || (BEHAVIORS[b] == "blue"))
      {
        postSynapticNeuron = BEHAVIORS[b] + "Response" + std::to_string(i);
      }
      else
      {
        postSynapticNeuron = BEHAVIORS[b] + "Response" + std::to_string((NETWORK_SIZE / 2) + i);
      }

      d_connectionMap[preSynapticNeuron].push_back({postSynapticNeuron, INPUT_RESPONSE_PULSE});

      // Register the connections between all neurons of the response layer and auxiliary layer
      for(int j = i; j < NETWORK_SIZE / 2; j++)
      {
        preSynapticNeuron = BEHAVIORS[b] + "Response" + std::to_string(i);
        postSynapticNeuron = BEHAVIORS[b] + "Auxiliary" + std::to_string(j);
        d_connectionMap[preSynapticNeuron].push_back({postSynapticNeuron, RESPONSE_AUX_PULSE});
      }
    }

    // Register the connections between all neurons of the input layer and response layer
    for(int i = NETWORK_SIZE - 1; i >= NETWORK_SIZE / 2; i--)
    {
      //preSynapticNeuron = BEHAVIORS[b] + "Input" + std::to_string(i);
      preSynapticNeuron = BEHAVIORS[b] + "Input" + std::to_string(i- (NETWORK_SIZE / 2));

      // Determine in the connection crosses the midline or not
      if((BEHAVIORS[b] == "red") || (BEHAVIORS[b] == "blue"))
      {
        postSynapticNeuron = BEHAVIORS[b] + "Response" + std::to_string(i);
      }
      else
      {
        postSynapticNeuron = BEHAVIORS[b] + "Response" + std::to_string(i- (NETWORK_SIZE / 2));
      }

      d_connectionMap[preSynapticNeuron].push_back({postSynapticNeuron, INPUT_RESPONSE_PULSE});

      // Register the connections between all neurons of the response layer and auxiliary layer
      for (int j = i; j >= NETWORK_SIZE / 2; j--)
      {
        preSynapticNeuron = BEHAVIORS[b] + "Response" + std::to_string(i);
        postSynapticNeuron = BEHAVIORS[b] + "Auxiliary" + std::to_string(j);
        d_connectionMap[preSynapticNeuron].push_back({postSynapticNeuron, RESPONSE_AUX_PULSE});
      }
    }

    // Connect the response layer neurons to all other neurons of the same layer with inhibitory connections.
    for(int i = 0; i < NETWORK_SIZE; i++)
    {
      for(int j = 0; j < NETWORK_SIZE; j++)
      {
        if(i != j)
        {
          // A response neuron shouldn't inhibit itself. Inhibit all other response neurons of the same behavior group
          std::string preSynapticNeuron = BEHAVIORS[b] + "Response" + std::to_string(i);
          std::string postSynapticNeuron = BEHAVIORS[b] + "Response" + std::to_string(j);
          d_connectionMap[preSynapticNeuron].push_back({postSynapticNeuron, -RESPONSE_LATERAL_PULSE});
        }
      }
    }
  }
}

/******************************************************************************/

void VisuomotorCenter::CreateConnections(BasalGanglia* BG)
{
  for(int i = 0; i < NETWORK_SIZE; i++)
  {
    for(unsigned int l = 0; l < BEHAVIORS.size(); l++)
    {
      // Connect the response layer of the Visuomotor Center to the STN neurons of the Basal Ganglia
      std::string preSynapticNeuron = BEHAVIORS[l] + "Response" + std::to_string(i);
      Neuron* postSynapticNeuron = BG->p_neurons[BEHAVIORS[l] + "STN"];
      p_neurons[preSynapticNeuron]->ConnectPostSynapticNeuron(postSynapticNeuron, RESPONSE_STN_PULSE);
    }
  }
}

/******************************************************************************/

void VisuomotorCenter::CreateConnections(Brainstem* B)
{
  for(int i = 0; i < NETWORK_SIZE/2; i++)
  {
    // Connect the neurons of the response layer to the locomotor neuron of the same side
    p_neurons["redResponse" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftLocomotor"], RESPONSE_LOCO_PULSE);
    p_neurons["blueResponse" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftLocomotor"], RESPONSE_LOCO_PULSE);
    p_neurons["greenResponse" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftLocomotor"], RESPONSE_LOCO_PULSE);

    // Connect the auxiliary neurons corresponding to the left eye to the reticulospinal neurons
    //p_neurons["redAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightReticulospinal"], PREDATOR_SCALE * AUX_RETICULO_PULSE);
    //p_neurons["blueAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightReticulospinal"], OBSTACLE_SCALE * AUX_RETICULO_PULSE);
    //p_neurons["greenAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftReticulospinal"], PREY_SCALE * AUX_RETICULO_PULSE);
    p_neurons["redAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftReticulospinal"], PREDATOR_SCALE * AUX_RETICULO_PULSE);
    p_neurons["blueAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftReticulospinal"], OBSTACLE_SCALE * AUX_RETICULO_PULSE);
    p_neurons["greenAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftReticulospinal"], PREY_SCALE * AUX_RETICULO_PULSE);
  }

  for(int i = NETWORK_SIZE - 1; i >= NETWORK_SIZE / 2; i--)
  {
    // Connect the neurons of the response layer to the locomotor neuron of the same side
    p_neurons["redResponse" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightLocomotor"], RESPONSE_LOCO_PULSE);
    p_neurons["blueResponse" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightLocomotor"], RESPONSE_LOCO_PULSE);
    p_neurons["greenResponse" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightLocomotor"], RESPONSE_LOCO_PULSE);

    // Connect the auxiliary neurons corresponding to the left eye to the reticulospinal neurons
    //p_neurons["redAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftReticulospinal"], PREDATOR_SCALE * AUX_RETICULO_PULSE);
    //p_neurons["blueAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["leftReticulospinal"], OBSTACLE_SCALE * AUX_RETICULO_PULSE);
    //p_neurons["greenAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightReticulospinal"], PREY_SCALE * AUX_RETICULO_PULSE);
    p_neurons["redAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightReticulospinal"], PREDATOR_SCALE * AUX_RETICULO_PULSE);
    p_neurons["blueAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightReticulospinal"], OBSTACLE_SCALE * AUX_RETICULO_PULSE);
    p_neurons["greenAuxiliary" +  std::to_string(i)]->ConnectPostSynapticNeuron(B->p_neurons["rightReticulospinal"], PREY_SCALE * AUX_RETICULO_PULSE);
  }
}

/******************************************************************************/

void VisuomotorCenter::Update(std::string layer)
{
  // Update the neurons of the specified layer for each behavior (red, blue, green)
  for(auto iter_behavior = BEHAVIORS.begin(); iter_behavior != BEHAVIORS.end(); iter_behavior++)
  {
    for (int i = 0; i < NETWORK_SIZE; i++)
    {
      p_neurons[ (*iter_behavior) + layer + std::to_string(i)]->UpdateNeuronState();
    }
  }
}

/******************************************************************************/

void VisuomotorCenter::Arbitrate()
{
  // Update the neurons of the response layer, but only allow transmission to the basal ganglia
  for(auto iter_behavior = BEHAVIORS.begin(); iter_behavior != BEHAVIORS.end(); iter_behavior++)
  {
    for (int i = 0; i < NETWORK_SIZE; i++)
    {
      std::string preSynapticNeuron = (*iter_behavior) + "Response" + std::to_string(i);
      std::string postSynapticNeuron = (*iter_behavior) + "STN";
      p_neurons[preSynapticNeuron]->TransmitMembranePotential(postSynapticNeuron);
    }
  }
}
