#ifndef EYE_H
#define EYE_H

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "xiApiPlusOcv.hpp"
#include "parameters.h"


// Camera Parameters
#define FRAME_WIDTH 648
#define FRAME_HEIGHT 486
#define OFFSET_X 0
#define OFFSET_Y 0


/***************************************************************************
*  @brief: An enumerated type to specify eye orientation
*
*  @details: An eye can be either on the right side of the robot, or on the left
*            side of the robot.
*
*            We can easily extend this to include a camera on the bottom
***************************************************************************/
enum Orientation {
  Left,
  Right
};



class Eye{


private:


  /***************************************************************************
  *  @brief: A camera object.
  ***************************************************************************/
  xiAPIplusCameraOcv cam;

  /***************************************************************************
  *  @brief: A string holding the camera's serial number
  *
  *  @details: For the system currently in use, the serial numbers are:
  *             - Camera 1: 25795659
  *             - Camera 2: 25794959
  ***************************************************************************/
  std::string CAMERA_SERIAL_NUMBER;

  /***************************************************************************
  *  @brief: Integers representing the eye's field of vision (FOV). This is
  *          specified terms of the number of vertical and horizontal pixels.
  ***************************************************************************/
  int FOV_HEIGHT;
  int FOV_WIDTH;

  /***************************************************************************
  *  @brief: A constant specifying the eye's orientation
  ***************************************************************************/
  Orientation EYE_ORIENTATION;

  /***************************************************************************
  *  @brief: The min and maximum object diameter detectible
  ***************************************************************************/
  double MAXIMUM_OBSTACLE_DIAMETER;
  double MINIMUM_OBSTACLE_DIAMETER;

  /***************************************************************************
  *  @brief: A calibration data file the eye can access to undistort images
  ***************************************************************************/
  std::string CALIBRATION_DATA_FILE;

  /***************************************************************************
  *  @brief: The intrinsic + extrinsic camera parameters, and distortion
  *          coefficients used to undistort the images.
  ***************************************************************************/
  cv::Mat CAMERA_MATRIX;
  cv::Mat DISTORTION_COEFFICIENTS;

  /***************************************************************************
  *  @brief: A simple blob detector object
  ***************************************************************************/
  cv::SimpleBlobDetector::Params d_blobDetectorParams;

  /***************************************************************************
  *  @brief: The parameters of the simple blob detector. These can be modified
  *          to specify what types of objects will be detected.
  ***************************************************************************/
  cv::Ptr<cv::SimpleBlobDetector> d_blobDetector;

  // An eye has a set of keypoints to define the coordinates and size of detected objects
  std::vector<cv::KeyPoint> d_redObjectKeypoints;
  std::vector<cv::KeyPoint> d_blueObjectKeypoints;
  std::vector<cv::KeyPoint> d_greenObjectKeypoints;

  // At each instant, an eye sees an image, its undistorted version, and its projection from the BGR to the HSV color space.
  cv::Mat d_currentRawFrame;
  cv::Mat d_currentUndistortedFrame;
  cv::Mat d_hsvImage;

public:

  // Default constructor.
  Eye();

  // Custom constructor
  Eye(Orientation s, std::string serialNumber);

  // Method to "open" the eye - which is to say, initialize the camera and start the video stream.
  void OpenEye();

  // Method for the eye to capture a new image. This updates the contents of the currentFrame matrix.
  void GetNewImage();

  // Method to "close" the eye - which is to say, stop the video stream and close the camera.
  void CloseEye();

  // Method to detect red, blue, and green objects. This operates on the contents of the currentFrame matrix.
  void DetectObjects(bool plot);

  // Method to generate the stimuli that will be applied to the neurons of the visuomotor center.
  void UpdateStimVectors(std::vector<double> &redStimVector, std::vector<double> &blueStimVector, std::vector<double> &greenStimVector);

  // Method to report the eye side
  Orientation EyeOrientation() { return EYE_ORIENTATION; }

  // Method to set the eye orientation
  void SetOrientation(Orientation o) { EYE_ORIENTATION = o; }

  // Method to report the eye serial number
  std::string SerialNumber() { return CAMERA_SERIAL_NUMBER; }

  // Method to set the eye serial number
  void SetSerialNumber(std::string SN) { CAMERA_SERIAL_NUMBER = SN; }

  // Method to return the current eye image.
  cv::Mat CurrentFrame();

  // Method to plot an outline about objects the eye sees in the current frame
  void DrawOutline (cv::Mat mask, std::vector<cv::KeyPoint> keypoints);
};

#endif
