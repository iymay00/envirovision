#ifndef PARAMETERS_H
#define PARAMETERS_H

/* NEURON PARAMETERS */
#define RP 0.0                                    // The neuron resting potential
#define DR 0.1                                    // The default decay rate
#define RS 2                                      // The rectification slope
#define SATURATION 1.0                            // The maximum firing rate value that the neuron can have
#define REFRACTORY_PERIOD 1000                    // The refractory period (microseconds).
#define FORGETTING_FACTOR 0.1                     // The forgetting factor used to decay the post synaptic potentials


/* NETWORK PARAMETERS */
#define NETWORK_SIZE 20                           // The number of neurons in a layer

#define INPUT_SPIKE_THRESH 1                    // Voltage threhsold for input neurons
#define INPUT_TIME_CONSTANT 1.0                   // Time constant for input neurons

#define RESPONSE_SPIKE_THRESH 0.2                 // Voltage threhsold for response neurons
#define RESPONSE_TIME_CONSTANT 1                // Time constant for response neurons

#define AUX_SPIKE_THRESH 0.4                      // Voltage threhsold for auxiliary neurons
#define AUX_TIME_CONSTANT 1                     // Time constant for auxiliary neurons

#define LOCO_SPIKE_THRESH 0.5                     // Voltage threhsold for locomotor neurons
#define LOCO_TIME_CONSTANT 1                    // Time constant for locomotor neurons

#define RETICULO_SPIKE_THRESH 0.6                 // Voltage threhsold for reticulospinal neurons
#define RETICULO_TIME_CONSTANT 1                  // Time constant for reticulospinal neurons

#define ARBITRATION_SPIKE_THRESH 0.5              // Voltage threhsold for basal ganglia neurons
#define ARBITRATION_TIME_CONSTANT 1             // Time constant for basal ganglia neurons

// Synaptic Strengths
#define INPUT_RESPONSE_PULSE 1.0
#define RESPONSE_AUX_PULSE 1.0
#define RESPONSE_LOCO_PULSE 1.0
#define AUX_RETICULO_PULSE 1.0
#define LOCO_RETICULO_PULSE 1.0
#define RESPONSE_LATERAL_PULSE 1.0//0.2
#define RESPONSE_STN_PULSE 1.0
#define GPI_RESPONSE_PULSE 1.0
#define STN_GPE_PULSE 1.0
#define STN_GPI_PULSE 1.0
#define GPE_GPI_PULSE 1.0

/* IMPLEMENTATION PARAMETERS */
#define MIN_DETECTION_SIZE 10                     // Minimum object diameter in pixels. Used to filter out objects that are either too far away, or that are noise.


#endif
