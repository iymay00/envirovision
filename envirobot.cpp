#include "envirobot.hpp"

/******************************************************************************/

Envirobot::Envirobot():
                      d_eyes(1),
                      d_redStimulationVector(NETWORK_SIZE, 0),
                      d_blueStimulationVector(NETWORK_SIZE, 0),
                      d_greenStimulationVector(NETWORK_SIZE, 0)
{
  // Create pointers to the brainstem basal ganglia and visuomotor center
  p_brainstem = new Brainstem();
  p_basalGanglia = new BasalGanglia();
  p_visuomotorCenter = new VisuomotorCenter();
}

/******************************************************************************/

Envirobot::Envirobot(std::vector<std::pair<Orientation, std::string>> cameraInfo):
                      d_eyes(cameraInfo.size()),
                      d_redStimulationVector(NETWORK_SIZE, 0),
                      d_blueStimulationVector(NETWORK_SIZE, 0),
                      d_greenStimulationVector(NETWORK_SIZE, 0)
{
  // Create pointers to the brainstem, basal ganglia, and visuomotor center
  p_brainstem = new Brainstem();
  p_basalGanglia = new BasalGanglia();
  p_visuomotorCenter = new VisuomotorCenter();

  // Assign a serial number and orientation to each of the eyes.
  for(unsigned int i = 0; i < cameraInfo.size(); i++)
  {
    // Extract the camera orientation and serial number from the pair object
    Orientation cameraOrientation = cameraInfo[i].first;
    std::string cameraSerialNumber = cameraInfo[i].second;

    // Assign the properties to each eye
    d_eyes[i].SetSerialNumber(cameraSerialNumber);
    d_eyes[i].SetOrientation(cameraOrientation);
  }
}

/******************************************************************************/

Envirobot::~Envirobot()
{
  // Free the pointers to the Brainstem Basal Ganglia and visuomotor center
  delete p_brainstem;
  delete p_basalGanglia;
  delete p_visuomotorCenter;
}

/******************************************************************************/

void Envirobot::CreateConnections()
{
  // Create the connections for neurons of the visuomotor center
  p_visuomotorCenter->CreateConnections();                                  // Interconnect visuomotor center neurons
  p_visuomotorCenter->CreateConnections(p_basalGanglia);                    // Connect visuomotor center to basal ganglia
  p_visuomotorCenter->CreateConnections(p_brainstem);                       // Connect visuomotor center to brainstem

  // Create the connections for neurons of the basal ganglia.
  p_basalGanglia->CreateConnections();                                      // Interconnect basal ganglia neurons
  p_basalGanglia->CreateConnections(p_visuomotorCenter);                    // Connect basal ganglia to visuomotor center.

  // Create the connections for neurons of the brainstem
  p_brainstem->CreateConnections();                                         // Interconnect brainstem neurons
}

/******************************************************************************/

void Envirobot::OpenEyes()
{
  // Initialize all eyes of the envirobot and start image acquisition
  for(auto iter_eye = d_eyes.begin(); iter_eye != d_eyes.end(); iter_eye++)
  {
    iter_eye->OpenEye();
  }
}

/******************************************************************************/

void Envirobot::CloseEyes()
{
  // Stop image acquisition and close all eyes of the envirobot.
  for(auto iter_eye = d_eyes.begin(); iter_eye != d_eyes.end(); iter_eye++)
  {
    iter_eye->CloseEye();
  }
}

/******************************************************************************/

void Envirobot::StimulateNetwork()
{
  // Stimulate the input neurons using the values in the stimulation vectors
  for (int i = 0; i < NETWORK_SIZE; i++)
  {
    p_visuomotorCenter->p_neurons["redInput" + std::to_string(i)]->StimulateNeuron(d_redStimulationVector[i]);
    p_visuomotorCenter->p_neurons["blueInput" + std::to_string(i)]->StimulateNeuron(d_blueStimulationVector[i]);
    p_visuomotorCenter->p_neurons["greenInput" + std::to_string(i)]->StimulateNeuron(d_greenStimulationVector[i]);
  }

  // Update the neurons of the visuomotor center's input layer
  p_visuomotorCenter->Update("Input");

  // Feed the signals from the response layer to the basal ganglia for arbitration
  p_visuomotorCenter->Arbitrate();

  // Update all neurons of the basal ganglia
  p_basalGanglia->Update();

  // Update the response layer to taking into account the arbitration of the basal ganglia
  p_visuomotorCenter->Update("Response");
  p_visuomotorCenter->Update("Auxiliary");

  // Update the neurons of the brainstem
  p_brainstem->Update();
}

/******************************************************************************/

void Envirobot::Cycle()
{
  // Iterate through all eyes connected to the envirobot
  for(auto iter_eye = d_eyes.begin(); iter_eye != d_eyes.end(); iter_eye++)
  {
    // Capture a new image with the eye.
    iter_eye->GetNewImage();

    // Detect objects in the eye's field of vision.
    iter_eye->DetectObjects(true);

    // Compute stimuli for the visuomotor center based on the location and size of objects in the eye's FOV.
    iter_eye->UpdateStimVectors(d_redStimulationVector, d_blueStimulationVector, d_greenStimulationVector);
  }

  // Propogate all action potentials throughout the network
  StimulateNetwork();

  // Compute the firing frequency for the output neurons.
  p_brainstem->OutputLeft();
  p_brainstem->OutputRight();

  // Visualize the neuron network
  DisplayNetwork();
}


/******************************************************************************/

cv::Mat Envirobot::CurrentFrames()
{
  // Structures to hold the temporary and merged frames
  cv::Mat leftFrame;
  cv::Mat rightFrame;
  cv::Mat mergedFrame;

  // Get the current image from each eye
  for(std::vector<Eye>::iterator iter_eye = d_eyes.begin(); iter_eye != d_eyes.end(); iter_eye++)
  {
    switch (iter_eye->EyeOrientation())
    {
      case Left :
        leftFrame = iter_eye->CurrentFrame();
        break;

      case Right :
        rightFrame = iter_eye->CurrentFrame();
        break;
    }
  }

  // Concatonate the images and return
  cv::hconcat(leftFrame, rightFrame, mergedFrame);
  return mergedFrame;
}

/******************************************************************************/

// A function to visualize the neuron network. Connections between neurons are not drawn, to reduce messiness.
// Neuron stimulation is visualized as proportional to color strength.
// Neuron stimulation values are displayed numerically beneath each neuron.
// The firing rates of the reticulo spinal neurons are displayed. These are the final outputs of the network.
void Envirobot::DisplayNetwork()
{
  // A set of parameters to control spacings between neurons and rows of neurons.
  const int CELL_RADIUS = 10;
  const int ROW_OFFSET = 40;
  const int COLUMN_OFFSET = 10;
  const int CELL_SPACING = 4;
  const double FONT_SIZE = 0.8;
  const int PRECISION = 2;

  // The image to be displayed
  cv::Mat networkImage(1000,2000, CV_8UC3, cv::Scalar(0,0,0));

  // Containers for the cell and text coordinates
  int horizontalCoordinate;
  int verticalCoordinate;

  // Declare a string stream to print the values of the neuron stimulation with a specified precision
  std::stringstream valueString;

  // Draw the neurons of the visuomotor network
  for (int i = 0; i < NETWORK_SIZE; i++)
  {

    // Red neurons and text of the visuomotor center
    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = CELL_RADIUS + (ROW_OFFSET * 1);
    cv::circle(networkImage, cv::Point(horizontalCoordinate, verticalCoordinate), CELL_RADIUS, cv::Scalar((255-p_visuomotorCenter->p_neurons["redInput" + std::to_string(i)]->MembranePotential()*255), (255-p_visuomotorCenter->p_neurons["redInput" + std::to_string(i)]->MembranePotential()*255), 255), p_visuomotorCenter->p_neurons["redInput" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["redInput" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["redInput" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");

    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = CELL_RADIUS + (ROW_OFFSET * 2);
    cv::circle(networkImage, cv::Point(horizontalCoordinate, verticalCoordinate), CELL_RADIUS, cv::Scalar((255-p_visuomotorCenter->p_neurons["redResponse" + std::to_string(i)]->MembranePotential()*255), (255-p_visuomotorCenter->p_neurons["redResponse" + std::to_string(i)]->MembranePotential()*255), 255),p_visuomotorCenter->p_neurons["redResponse" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["redResponse" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["redResponse" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");

    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = CELL_RADIUS + (ROW_OFFSET * 3);
    cv::circle(networkImage, cv::Point(horizontalCoordinate, verticalCoordinate), CELL_RADIUS, cv::Scalar((255-p_visuomotorCenter->p_neurons["redAuxiliary" + std::to_string(i)]->MembranePotential()*255), (255-p_visuomotorCenter->p_neurons["redAuxiliary" + std::to_string(i)]->MembranePotential()*255), 255),p_visuomotorCenter->p_neurons["redAuxiliary" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["redAuxiliary" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["redAuxiliary" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");

    // Green neurons and text of the visuomotor center
    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = 275 + CELL_RADIUS + (ROW_OFFSET * 1);
    cv::circle(networkImage, cv::Point( horizontalCoordinate, verticalCoordinate), CELL_RADIUS, cv::Scalar((255-p_visuomotorCenter->p_neurons["greenInput" + std::to_string(i)]->MembranePotential()*255), 255, (255-p_visuomotorCenter->p_neurons["greenInput" + std::to_string(i)]->MembranePotential()*255)),p_visuomotorCenter->p_neurons["greenInput" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["greenInput" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["greenInput" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");

    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = 275 + CELL_RADIUS + (ROW_OFFSET * 2);
    cv::circle(networkImage, cv::Point(horizontalCoordinate, verticalCoordinate), CELL_RADIUS, cv::Scalar((255-p_visuomotorCenter->p_neurons["greenResponse" + std::to_string(i)]->MembranePotential()*255), 255, (255-p_visuomotorCenter->p_neurons["greenResponse" + std::to_string(i)]->MembranePotential()*255)),p_visuomotorCenter->p_neurons["greenResponse" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["greenResponse" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["greenResponse" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");

    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = 275 + CELL_RADIUS + (ROW_OFFSET * 3);
    cv::circle(networkImage, cv::Point(horizontalCoordinate, verticalCoordinate), CELL_RADIUS, cv::Scalar((255-p_visuomotorCenter->p_neurons["greenAuxiliary" + std::to_string(i)]->MembranePotential()*255), 255, (255-p_visuomotorCenter->p_neurons["greenAuxiliary" + std::to_string(i)]->MembranePotential()*255)),p_visuomotorCenter->p_neurons["greenAuxiliary" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["greenAuxiliary" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["greenAuxiliary" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");

    // Blue neurons and text of the visuomotor center
    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = 550 + CELL_RADIUS + (ROW_OFFSET * 1);
    cv::circle(networkImage, cv::Point(horizontalCoordinate, verticalCoordinate), CELL_RADIUS, cv::Scalar(255, (255-p_visuomotorCenter->p_neurons["blueInput" + std::to_string(i)]->MembranePotential()*255), (255-p_visuomotorCenter->p_neurons["blueInput" + std::to_string(i)]->MembranePotential()*255)),p_visuomotorCenter->p_neurons["blueInput" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["blueInput" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["blueInput" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");

    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = 550 + CELL_RADIUS + (ROW_OFFSET * 2);
    cv::circle(networkImage, cv::Point(horizontalCoordinate , verticalCoordinate), CELL_RADIUS, cv::Scalar(255, (255-p_visuomotorCenter->p_neurons["blueResponse" + std::to_string(i)]->MembranePotential()*255), (255-p_visuomotorCenter->p_neurons["blueResponse" + std::to_string(i)]->MembranePotential()*255)),p_visuomotorCenter->p_neurons["blueResponse" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["blueResponse" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["blueResponse" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");

    horizontalCoordinate = COLUMN_OFFSET + CELL_RADIUS + (CELL_RADIUS * i * CELL_SPACING);
    verticalCoordinate = 550 + CELL_RADIUS + (ROW_OFFSET * 3);
    cv::circle(networkImage, cv::Point(horizontalCoordinate, verticalCoordinate), CELL_RADIUS, cv::Scalar(255, (255-p_visuomotorCenter->p_neurons["blueAuxiliary" + std::to_string(i)]->MembranePotential()*255), (255-p_visuomotorCenter->p_neurons["blueAuxiliary" + std::to_string(i)]->MembranePotential()*255)),p_visuomotorCenter->p_neurons["blueAuxiliary" + std::to_string(i)]->MembranePotential()>=p_visuomotorCenter->p_neurons["blueAuxiliary" + std::to_string(i)]->FiringThreshold()?(CELL_RADIUS/2):-1);

    horizontalCoordinate = horizontalCoordinate - CELL_RADIUS;
    verticalCoordinate = verticalCoordinate + (2 * CELL_RADIUS);
    valueString << std::fixed << std::setprecision(PRECISION) << p_visuomotorCenter->p_neurons["blueAuxiliary" + std::to_string(i)]->MembranePotential();
    cv::putText(networkImage, valueString.str(), cv::Point(horizontalCoordinate, verticalCoordinate), cv::FONT_HERSHEY_PLAIN, FONT_SIZE, cv::Scalar(255,255,255));
    valueString.str("");
  }

  // Red neurons of the arbitrations system (basal ganglia)
  cv::circle(networkImage, cv::Point(1000, 200), CELL_RADIUS*2, cv::Scalar((255-p_basalGanglia->p_neurons["redSTN"]->MembranePotential()*255), (255-p_basalGanglia->p_neurons["redSTN"]->MembranePotential()*255), 255),p_basalGanglia->p_neurons["redSTN"]->MembranePotential()>=p_basalGanglia->p_neurons["redSTN"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  putText(networkImage, std::to_string(p_basalGanglia->p_neurons["redSTN"]->MembranePotential()),cv::Point(1000-CELL_RADIUS, 200+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  cv::circle(networkImage, cv::Point(950, 225), CELL_RADIUS*2, cv::Scalar((255-p_basalGanglia->p_neurons["redGPi"]->MembranePotential()*255), (255-p_basalGanglia->p_neurons["redGPi"]->MembranePotential()*255), 255),p_basalGanglia->p_neurons["redGPi"]->MembranePotential()>=p_basalGanglia->p_neurons["redGPi"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  putText(networkImage, std::to_string(p_basalGanglia->p_neurons["redGPi"]->MembranePotential()),cv::Point(950-CELL_RADIUS, 225+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  cv::circle(networkImage, cv::Point(1050, 225), CELL_RADIUS*2, cv::Scalar((255-p_basalGanglia->p_neurons["redGPe"]->MembranePotential()*255), (255-p_basalGanglia->p_neurons["redGPe"]->MembranePotential()*255), 255),p_basalGanglia->p_neurons["redGPe"]->MembranePotential()>=p_basalGanglia->p_neurons["redGPe"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  putText(networkImage, std::to_string(p_basalGanglia->p_neurons["redGPe"]->MembranePotential()),cv::Point(1050-CELL_RADIUS, 225+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  // Green neurons of the arbitrations system (basal ganglia)
  circle(networkImage, cv::Point(1000,475), CELL_RADIUS*2, cv::Scalar((255-p_basalGanglia->p_neurons["greenSTN"]->MembranePotential()*255), 255, (255-p_basalGanglia->p_neurons["greenSTN"]->MembranePotential()*255)),p_basalGanglia->p_neurons["greenSTN"]->MembranePotential()>=p_basalGanglia->p_neurons["greenSTN"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  putText(networkImage, std::to_string(p_basalGanglia->p_neurons["greenSTN"]->MembranePotential()),cv::Point(1000-CELL_RADIUS, 475+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  circle(networkImage, cv::Point(950,500), CELL_RADIUS*2, cv::Scalar((255-p_basalGanglia->p_neurons["greenGPi"]->MembranePotential()*255), 255, (255-p_basalGanglia->p_neurons["greenGPi"]->MembranePotential()*255)),p_basalGanglia->p_neurons["greenGPi"]->MembranePotential()>=p_basalGanglia->p_neurons["greenGPi"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  cv::putText(networkImage, std::to_string(p_basalGanglia->p_neurons["greenGPi"]->MembranePotential()),cv::Point(950-CELL_RADIUS, 500+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  circle(networkImage, cv::Point(1050,500), CELL_RADIUS*2, cv::Scalar((255-p_basalGanglia->p_neurons["greenGPe"]->MembranePotential()*255), 255, (255-p_basalGanglia->p_neurons["greenGPe"]->MembranePotential()*255)),p_basalGanglia->p_neurons["greenGPe"]->MembranePotential()>=p_basalGanglia->p_neurons["greenGPe"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  cv::putText(networkImage, std::to_string(p_basalGanglia->p_neurons["greenGPe"]->MembranePotential()),cv::Point(1050-CELL_RADIUS, 500+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  // Blue neurons of the arbitrations system (basal ganglia)
  circle(networkImage, cv::Point(1000,750), CELL_RADIUS*2, cv::Scalar(255, (255-p_basalGanglia->p_neurons["blueSTN"]->MembranePotential()*255), (255-p_basalGanglia->p_neurons["blueSTN"]->MembranePotential()*255)),p_basalGanglia->p_neurons["blueSTN"]->MembranePotential()>=p_basalGanglia->p_neurons["blueSTN"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  cv::putText(networkImage, std::to_string(p_basalGanglia->p_neurons["blueSTN"]->MembranePotential()),cv::Point(1000-CELL_RADIUS, 750+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  circle(networkImage, cv::Point(950,775), CELL_RADIUS*2, cv::Scalar(255, (255-p_basalGanglia->p_neurons["blueGPi"]->MembranePotential()*255), (255-p_basalGanglia->p_neurons["blueGPi"]->MembranePotential()*255)),p_basalGanglia->p_neurons["blueGPi"]->MembranePotential()>=p_basalGanglia->p_neurons["blueGPi"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  cv::putText(networkImage, std::to_string(p_basalGanglia->p_neurons["blueGPi"]->MembranePotential()),cv::Point(950-CELL_RADIUS, 775+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  circle(networkImage, cv::Point(1050,775), CELL_RADIUS*2, cv::Scalar(255, (255-p_basalGanglia->p_neurons["blueGPe"]->MembranePotential()*255), (255-p_basalGanglia->p_neurons["blueGPe"]->MembranePotential()*255)),p_basalGanglia->p_neurons["blueGPe"]->MembranePotential()>=p_basalGanglia->p_neurons["blueGPe"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  cv::putText(networkImage, std::to_string(p_basalGanglia->p_neurons["blueGPe"]->MembranePotential()),cv::Point(1050-CELL_RADIUS, 775+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  // The locomotor neurons
  circle(networkImage, cv::Point(950,850), CELL_RADIUS*2, cv::Scalar(255, (255-p_brainstem->p_neurons["leftLocomotor"]->MembranePotential()*255), 255),p_brainstem->p_neurons["leftLocomotor"]->MembranePotential()>=p_brainstem->p_neurons["leftLocomotor"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  cv::putText(networkImage, std::to_string(p_brainstem->p_neurons["leftLocomotor"]->MembranePotential()),cv::Point(950-CELL_RADIUS, 850+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  circle(networkImage, cv::Point(1050,850), CELL_RADIUS*2, cv::Scalar(255, (255-p_brainstem->p_neurons["rightLocomotor"]->MembranePotential()*255), 255),p_brainstem->p_neurons["rightLocomotor"]->MembranePotential()>=p_brainstem->p_neurons["rightLocomotor"]->FiringThreshold()?(CELL_RADIUS/2):-1);
  cv::putText(networkImage, std::to_string(p_brainstem->p_neurons["rightLocomotor"]->MembranePotential()),cv::Point(1050-CELL_RADIUS, 850+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255));

  // The reticulo-spinal neurons
  cv::circle(networkImage, cv::Point(950,950), CELL_RADIUS*2, cv::Scalar(255, (255-p_brainstem->p_neurons["leftReticulospinal"]->MembranePotential()*255), 255),p_brainstem->p_neurons["leftReticulospinal"]->MembranePotential()>=p_brainstem->p_neurons["leftReticulospinal"]->FiringThreshold()?(CELL_RADIUS/2):-1);

  valueString << std::fixed << std::setprecision(PRECISION) << p_brainstem->OutputLeft();
  cv::putText(networkImage, valueString.str() + " Hz", cv::Point(950-CELL_RADIUS, 950+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.2, cv::Scalar(255,255,255));
  valueString.str("");

  cv::putText(networkImage, std::to_string(p_brainstem->p_neurons["leftReticulospinal"]->MembranePotential()), cv::Point(800-CELL_RADIUS, 950), cv::FONT_HERSHEY_PLAIN, 1.2, cv::Scalar(255,255,255));


  cv::circle(networkImage, cv::Point(1050,950), CELL_RADIUS*2, cv::Scalar(255, (255-p_brainstem->p_neurons["rightReticulospinal"]->MembranePotential()*255), 255),p_brainstem->p_neurons["rightReticulospinal"]->MembranePotential()>=p_brainstem->p_neurons["rightReticulospinal"]->FiringThreshold()?(CELL_RADIUS/2):-1);

  valueString << std::fixed << std::setprecision(PRECISION) << p_brainstem->OutputRight();
  cv::putText(networkImage, valueString.str() + " Hz", cv::Point(1050-CELL_RADIUS, 950+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.2, cv::Scalar(255,255,255));
  valueString.str("");

  cv::putText(networkImage, std::to_string(p_brainstem->p_neurons["rightReticulospinal"]->MembranePotential()), cv::Point(1100-CELL_RADIUS, 950), cv::FONT_HERSHEY_PLAIN, 1.2, cv::Scalar(255,255,255));

  // Report the imbalance between left and right neurons
  valueString << std::fixed << std::setprecision(PRECISION) << p_brainstem->OutputLeft() - p_brainstem->OutputRight();
  cv::putText(networkImage, valueString.str() + " Hz", cv::Point(100-CELL_RADIUS, 950+CELL_RADIUS*4), cv::FONT_HERSHEY_PLAIN, 1.2, cv::Scalar(255,255,255));
  valueString.str("");

  // Display the image
  cv::imshow("Visual Network", networkImage);
  cv::waitKey(1);
}
