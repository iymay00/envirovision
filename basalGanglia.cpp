#include "basalGanglia.hpp"

/******************************************************************************/

BasalGanglia::BasalGanglia ()
{
  // Create the behaviors that the robot can exhibit.
  BEHAVIORS.push_back("green");
  BEHAVIORS.push_back("blue");
  BEHAVIORS.push_back("red");

  // Create the possible types of neurons a cell can be.
  CELL_TYPES.push_back("STN");
  CELL_TYPES.push_back("GPe");
  CELL_TYPES.push_back("GPi");

  // Create the neurons of the Basal Ganglia
  for(unsigned int b = 0; b < BEHAVIORS.size(); b++)
  {
    for(unsigned int t = 0; t < CELL_TYPES.size(); t++)
    {
      //Each neuron is given a name based on its behavior and cell type
      std::string neuronName = BEHAVIORS[b] + CELL_TYPES[t];
      p_neurons[neuronName] = new Neuron(ARBITRATION_SPIKE_THRESH, ARBITRATION_TIME_CONSTANT, neuronName);
    }
  }

  // Register the neurons connected to the red STN neuron and the pulse strength between them
  d_connectionMap["redSTN"] = {
                                  {"redGPe", STN_GPE_PULSE},
                                  {"blueGPi", STN_GPI_PULSE},
                                  {"greenGPi", STN_GPI_PULSE}
                              };

  // Register the neurons connected to the blue STN neuron and the pulse strength between them
  d_connectionMap["blueSTN"] = {
                                  {"blueGPe", STN_GPE_PULSE},
                                  {"redGPi", STN_GPI_PULSE},
                                  {"greenGPi", STN_GPI_PULSE}
                               };

  // Register the neurons connected to the green STN neuron and the pulse strength between them
  d_connectionMap["greenSTN"] = {
                                  {"greenGPe", STN_GPE_PULSE},
                                  {"redGPi", STN_GPI_PULSE},
                                  {"blueGPi", STN_GPI_PULSE}
                                };

  // Register the neurons connected to the red GPe neuron and the pulse strength between them
  d_connectionMap["redGPe"] = {
                                  {"blueSTN", -STN_GPE_PULSE},
                                  {"greenSTN", -STN_GPE_PULSE},
                                  {"redGPi", -GPE_GPI_PULSE}
                              };

  // Register the neurons connected to the blue GPe neuron and the pulse strength between them
  d_connectionMap["blueGPe"] = {
                                  {"greenSTN", -STN_GPE_PULSE},
                                  {"redSTN", -STN_GPE_PULSE},
                                  {"blueGPi", -GPE_GPI_PULSE}
                               };

  // Register the neurons connected to the green GPe neuron and the pulse strength between them
  d_connectionMap["greenGPe"] = {
                                  {"redSTN", -STN_GPE_PULSE},
                                  {"blueSTN", -STN_GPE_PULSE},
                                  {"greenGPi", -GPE_GPI_PULSE}
                                };

  // Register the neurons connected to the red GPi neuron and the pulse strength between them
  d_connectionMap["redGPi"] = {
                                {"redSTN", -STN_GPI_PULSE}
                              };

  // Register the neurons connected to the blue GPi neuron and the pulse strength between them
  d_connectionMap["blueGPi"] = {
                                  {"blueSTN", -STN_GPI_PULSE}
                               };

  // Register the neurons connected to the green GPi neuron and the pulse strength between them
  d_connectionMap["greenGPi"] = {
                                  {"greenSTN", -STN_GPI_PULSE}
                                };
}

/******************************************************************************/

void BasalGanglia::CreateConnections(VisuomotorCenter* VMC)
{
  // Connect the GPi neurons of the basal ganglia to the Response layer of the neurovisual system.
  for(int i = 0; i < NETWORK_SIZE; i++){

    // Connect GPi neurons to Response layer of Visuomotor Center
    p_neurons["redGPi"]->ConnectPostSynapticNeuron(VMC->p_neurons["redResponse" + std::to_string(i)], -GPI_RESPONSE_PULSE);
    p_neurons["blueGPi"]->ConnectPostSynapticNeuron(VMC->p_neurons["blueResponse" + std::to_string(i)], -GPI_RESPONSE_PULSE);
    p_neurons["greenGPi"]->ConnectPostSynapticNeuron(VMC->p_neurons["greenResponse" + std::to_string(i)], -GPI_RESPONSE_PULSE);
  }
}

/******************************************************************************/

void BasalGanglia::Update()
{
  p_neurons["redSTN"]->UpdateNeuronState();
  p_neurons["blueSTN"]->UpdateNeuronState();
  p_neurons["greenSTN"]->UpdateNeuronState();

  p_neurons["redGPe"]->UpdateNeuronState();
  p_neurons["blueGPe"]->UpdateNeuronState();
  p_neurons["greenGPe"]->UpdateNeuronState();

  p_neurons["redGPi"]->UpdateNeuronState();
  p_neurons["blueGPi"]->UpdateNeuronState();
  p_neurons["greenGPi"]->UpdateNeuronState();
}

/******************************************************************************/
