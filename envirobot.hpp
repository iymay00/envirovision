#ifndef ENVIROBOT_H
#define ENVIROBOT_H

#include "neuron.hpp"
#include "brainstem.hpp"
#include "eye.hpp"
#include "visuomotorCenter.hpp"
#include "basalGanglia.hpp"

// Forward declaration of the basal ganglia and visuomotor center classes.
class Brainstem;
class BasalGanglia;
class VisuomotorCenter;


class Envirobot {

private:

  /***************************************************************************
  *  @brief: A vector holding eye objects
  ***************************************************************************/
  std::vector<Eye> d_eyes;

  /***************************************************************************
  *  @brief: A pointer to the robot's basal ganglia
  ***************************************************************************/
  BasalGanglia* p_basalGanglia;

  /***************************************************************************
  *  @brief: A pointer to the robot's visuomotor center
  ***************************************************************************/
  VisuomotorCenter* p_visuomotorCenter;


  /***************************************************************************
  *  @brief: A pointer to the robot's brainstem
  ***************************************************************************/
  Brainstem* p_brainstem;


  /***************************************************************************
  *  @brief: A vector of stimulation values
  *
  *  @details: Stimulation vectors are used to stimulate the input neurons of
  *            the controller. The stimulation vectors represent the location
  *            and size of objects in the robot's environment.
  *
  * 		       These data structues behave as the sensory neurons. They
  *            do not fire action potentials, in line with this biological
  *            fact:
  *		          - https://biology.stackexchange.com/questions/28634/are-sensory-receptors-neurons
  ***************************************************************************/
  std::vector<double> d_redStimulationVector;
  std::vector<double> d_blueStimulationVector;
  std::vector<double> d_greenStimulationVector;

public:

  /***************************************************************************
  *  @brief: Default constructor
  *
  *  @details: Method to create pointers to all of the functional group
  *            objects. Creates an instance of the brainstem, basal ganglia,
  *            & visuomotor center.
  *
  *            Finds the first connected eye, and automatically assigns its
  *            orientation as being on the left side of the robot.
  *
  *            Registers pointers to all neurons of the robot in a vector.
  *
  *  @warning: This constructor works if only ONE camera is connected to the
  *            hardware. Otherwise, use the custom constructor.
  ***************************************************************************/
  Envirobot();

  /***************************************************************************
  *  @brief: Custom constructor
  *
  *  @details: Method to create pointers to all of the functional group
  *            objects. Creates an instance of the brainstem, basal ganglia,
  *            & visuomotor center.
  *
  *            Finds all connected eyes to assign their orientations & serial
  *            numbers.
  *
  *            Registers pointers to all neurons of the robot in a vector.
  *
  *  @params[in]: std::vector<std::pair<Orientation, std::string>>
  *                 - A vector of pairs specifying orientation and serial number
  *                   for each camera of the robot.
  ***************************************************************************/
  Envirobot(std::vector<std::pair<Orientation, std::string>> cameraInfo);

  /***************************************************************************
  *  @brief: Destructor
  *
  *  @details: Method to delete the pointers to all of the functional group
  *            objects. Deletes the brainstem, basal ganglia, & visuomotor
  *            center.
  ***************************************************************************/
  ~Envirobot();

  /***************************************************************************
  *  @brief: Create neron connections
  *
  *  @details: Method to create the neuron connections needed to
  *            implement the vision-based controller. This method
  *            creates the connections within functional groups (brainstem,
  *            basal ganglia, visuomotor center), as well as the connections
  *            linking the functional groups.
  ***************************************************************************/
  void CreateConnections();

  /***************************************************************************
  *  @brief: Initialize envirobot cameras
  *
  *  @details: Method to initialize all eyes of the envirobot and
  *            begin image acquisition
  ***************************************************************************/
  void OpenEyes();

  /***************************************************************************
  *  @brief: Shutdown envirobot cameras
  *
  *  @details: Method to close all eyes of the envirobot and
  *            stop image acquisition
  ***************************************************************************/
  void CloseEyes();

  /***************************************************************************
  *  @brief: Stimulates the neuron network
  *
  *  @details: Method to go stimulate the input neurons of the visuomotor Center
  *            using the stimulation vectors corresponding to each behavior.
  *
  *            After stimulating the input neurons, the state of each neuron in
  *            the network is updated. This causes the action potentials to
  *            propogate.
  ***************************************************************************/
  void StimulateNetwork();

  /***************************************************************************
  *  @brief: Carries out a full cycle of operations
  *
  *  @details: Method to go through all steps of the controller's
  *            processing cycle. This pipeline is composed of the following:
  *             1) Capture new image(s) ->
  *             2) Perform object detection ->
  *             3) Stimulate the neuron network ->
  *             4) Generate output signals
  ***************************************************************************/
  void Cycle();

  /***************************************************************************
  *  @brief: Gets the current camera frames and merges them into a single image
  *
  *  @details: Method to get the current camera frames for each camera, and
  *            merge them into a single frame.
  *
  *  @retval: cv::Mat
  *             - Object representing the merged frame
  ***************************************************************************/
  cv::Mat CurrentFrames();

  // Display all neurons in the network, and the output firing rates.
  void DisplayNetwork();
};

#endif
