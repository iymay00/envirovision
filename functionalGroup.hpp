#ifndef FUNCTIONALGROUP_H
#define FUNCTIONALGROUP_H

#include "neuron.hpp"
#include <map>
#include <thread>
#include "parameters.h"



/*******************************************************************************
*
*   The FunctionalGroup class contains the basic data structures and methods
*   common to the Basal Ganglia, Visuomotor Center, and Brainstem classes.
*
*******************************************************************************/

class FunctionalGroup{

protected:


  /***************************************************************************
  *  @brief: A vector listing all possible behaviors the robot may exhibit
  *
  *  @details: A functional group can arbitrate between several behaviors:
  *             - Approach --> "green"
  *             - Avoid -----> "blue"
  *             - Escape ----> "red"
  ***************************************************************************/
  std::vector<std::string> BEHAVIORS;


  /***************************************************************************
  *  @brief: A vector listing all possible cell types of the functional group
  *
  *  @details: A functional group may have various cell types:
  *             - Subthalamic nucleus neuron -------> "STN"
  *             - Internal globus pallidus neuron --> "GPi"
  *             - External globus pallidus neuron --> "GPe"
  *             - Locomotor Neuron -----------------> "Locomotor"
  *             - Reticulospinal Neuron ------------> "Reticulospinal"
  ***************************************************************************/
  std::vector<std::string> CELL_TYPES;


  /***************************************************************************
  *  @brief: A vector listing all possible layers of the functional group
  *
  *  @details: A functional group may have various layers.
  *             - Input Layer -------> "Input"
  *             - Response Layer ----> "Response"
  *             - Auxiliary ---------> "Auxiliary"
  ***************************************************************************/
  std::vector<std::string> LAYERS;


  /***************************************************************************
  *  @brief: A vector listing all sides of the robot
  *
  *  @details: Functional groups can have neurons on either the right or left sides.
  *             - Left ------> "left"
  *             - Right -----> "right"
  ***************************************************************************/
  std::vector<std::string> SIDES;


  /***************************************************************************
  *  @brief: A hash map holding lists of neuron names
  *
  *  @details: This dictionary maps a pre-synaptic neuron's name (string) to a
  *            vector listing information about all post-synaptic neurons.
  *
  *            Each entry of the vector is a pair. A pair is composed of a neuron
  *            name and the synaptic strength between the two neurons.
  *
  *             - Key...... Neuron name (std::string)
  *             - Value.... Vector of pairs (std::vector<std::pair <std::string, double>>>)
  ***************************************************************************/
  std::map<std::string, std::vector<std::pair <std::string, double>>> d_connectionMap;


public:


  /***************************************************************************
  *  @brief: A hash map holding pointers to neuron objects
  *
  *  @details: This dictionary maps a neuron's name (string) to a neuron pointer.
  *             - Key...... Neuron name (std::string)
  *             - Value.... Pointer to neuron object (Neuron*)
  ***************************************************************************/
  std::map<std::string, Neuron*> p_neurons;


  /***************************************************************************
  *  @brief: Default constructor
  *
  *  @details: This constructor does nothing but assign the filter width using an
  *            initializer list.
  ***************************************************************************/
  FunctionalGroup(){};


  /***************************************************************************
  *  @brief: Destructor
  *
  *  @details: Method to delete the pointers to all of the neurons held within
  *            the hash table registry.
  ***************************************************************************/
  virtual ~FunctionalGroup();


  /***************************************************************************
  *  @brief: Creates neuron connections
  *
  *  @details: Interconnects the constituent neurons of a single functional group.
  *            This method uses the connection map hash table to identify the
  *            post-synaptic neurons that must be connected to each pre-synaptic
  *            neuron
  ***************************************************************************/
  void CreateConnections();
};

#endif
