# Envirovision

The envirovision software implements a biologically inspired visuomotor controller for anguilliform swimming robots. The current implementation produces a pair of left and right output signals, which are used to drive a Central Pattern Generator (CpG). The software allows the anguilliform swimming robot to autonomously swim around while avoiding obstacles, escaping predators, and approaching prey.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system. Some adjustments may be needed in the image capture, image processing, and blob detection before the robot will properly track targets in its field of view.

### Prerequisites

Packages and libraries required to run the envirovision software on the NanoPi NEO Plus2:

[XIMEA Linux Software Package](https://www.ximea.com/support/wiki/apis/XIMEA_Linux_Software_Package)

[OpenCV Version 3.3.1](https://opencv.org/)

[HTTP streaming library](https://c4science.ch/diffusion/5709/cscorelibs_envirovision) (included in this project)

FFmpeg (sudo apt-get install ffmpeg)

v4l-utils (sudo apt-get install v4l-utils)

Eigen (sudo apt install libeigen3-dev)

ccmake (sudo apt-get install cmake-curses-gui)

### Installing

Install the following libraries on your machine _before_ installing OpenCV.

```
- sudo apt-get install ffmpeg
- sudo apt-get install v4l-utils
- sudo apt install libeigen3-dev
- sudo apt-get install cmake-curses-gui
- XIMEA Linux Software Package (follow the instructions in the above link to the XIMEA Linux Software Package webpage)
```

Install OpenCV. The following [instructions](https://www.learnopencv.com/install-opencv3-on-ubuntu/) may be useful. These installation instructions were used when developing the system, and are known to work. Only the section on setting up a Virtual Environment to install Python libraries was ignored. Also, we used **ccmake** instead of **cmake** in order to make use of its graphical interface when setting the following flags.

```
- WITH_XIMEA = ON
- WITH_V4L = ON
- WITH_EIGEN = ON
- WITH_LIBV4L = ON
```


Then, navigate to the envirovision directory and run the makefile

```
- make
```

The resulting executable can be run to drive the robot.

```
sudo ./envirovision
```


## Installation Notes

- If compiling OpenCV on a NanoPi NEO Plus2 proves difficult (due to system hanging), it may be useful to limit the CPU frequency to 0.9GHz using [cpufrequtils](http://www.thinkwiki.org/wiki/How_to_use_cpufrequtils)

- Compilation may fail if done with multiple cores (make -jX). If this is the case, compile with only one core (make -j1)

## Logging and Visualization

- To see the video stream taken by the Robot, set the flag HTTP_STREAM = 1 in the file "envirovision.cpp"
- On the monitoring PC, open http://staticIP:8000 (for example, if the current IP is set to 192.168.21.135 on Biorob Local, navigate to http://192.168.21.135:8000 in the browser). The remote PC must also be connected to Biorob Local.

## Authors

* **Ibrahim Youssef**
* **Roger Fong** - *Initial work* - [envirovision prototype code](https://c4science.ch/diffusion/5708/envirovision_nanopi.git)


## Acknowledgments

* The code for communicating signals to the CpG was developed by Alessandro Crespi and Behzad Bayat.
