#include "brainstem.hpp"

/******************************************************************************/

Brainstem::Brainstem():
                      d_leftOutputFilter(10, 0),
                      d_rightOutputFilter(10, 0)
{
  // Create the possible sides the neurons can be on.
  SIDES.push_back("left");
  SIDES.push_back("right");

  // Create the possible types of neurons a cell can be.
  CELL_TYPES.push_back("Locomotor");
  CELL_TYPES.push_back("Reticulospinal");

  // Create the neurons of the Brainstem
  for(unsigned int i = 0; i < SIDES.size(); i++)
  {
    for(unsigned int j = 0; j < CELL_TYPES.size(); j++)
    {
      //Each neuron is given a name based on its behavior and cell type
      std::string neuronName = SIDES[i] + CELL_TYPES[j];
      p_neurons[neuronName] = new Neuron(ARBITRATION_SPIKE_THRESH, ARBITRATION_TIME_CONSTANT, neuronName);
    }
  }

  // Register the neurons connected to the left Locomotor neuron and the pulse strength between them
  d_connectionMap["leftLocomotor"] = {
                                        {"leftReticulospinal", LOCO_RETICULO_PULSE},
                                        {"rightReticulospinal", LOCO_RETICULO_PULSE}
                                     };

  // Register the neurons connected to the right Locomotor neuron and the pulse strength between them
  d_connectionMap["rightLocomotor"] = {
                                        {"rightReticulospinal", LOCO_RETICULO_PULSE},
                                        {"leftReticulospinal", LOCO_RETICULO_PULSE}
                                      };

}

/******************************************************************************/

void Brainstem::Update()
{
  p_neurons["leftLocomotor"]->UpdateNeuronState();
  p_neurons["rightLocomotor"]->UpdateNeuronState();
  p_neurons["leftReticulospinal"]->UpdateNeuronState();
  p_neurons["rightReticulospinal"]->UpdateNeuronState();

  // Compute the frequency of the reticulospinal neurons, and feed to the output filters
  d_leftOutputFilter.push_front(p_neurons["leftReticulospinal"]->FiringRate());
  d_rightOutputFilter.push_front(p_neurons["rightReticulospinal"]->FiringRate());

  // Pop the oldest elements
  d_leftOutputFilter.pop_back();
  d_rightOutputFilter.pop_back();

  // Average the contents of the filter and return the value
  d_outputLeft = std::accumulate(d_leftOutputFilter.begin(), d_leftOutputFilter.end(), 0.0);
  d_outputLeft = d_outputLeft / d_leftOutputFilter.size();

  d_outputRight = std::accumulate(d_rightOutputFilter.begin(), d_rightOutputFilter.end(), 0.0);
  d_outputRight = d_outputRight / d_rightOutputFilter.size();
}

/******************************************************************************/
