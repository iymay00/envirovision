#ifndef BRAINSTEM_H
#define BRAINSTEM_H

#include "functionalGroup.hpp"

class Brainstem: public FunctionalGroup {


private:

  /***************************************************************************
  *  @brief: Mean filters implemented using FIFO list
  *
  *  @details: These are a set of low pass filters to reduce
  *            rapid variations in the output signal.
  ***************************************************************************/
  std::deque<double> d_leftOutputFilter;
  std::deque<double> d_rightOutputFilter;


  /***************************************************************************
  *  @brief: Output variables to control the CpG
  ***************************************************************************/
  double d_outputLeft;
  double d_outputRight;


public:


  /***************************************************************************
  *  @brief: Default constructor
  *
  *  @details: Method to create all neurons of the brainstem by assigning them
  *            unique names based on their properties.
  *
  *            Registers the connections between the different neurons of the
  *            brainstem.
  ***************************************************************************/
  Brainstem();


  /***************************************************************************
  *  @brief: Updates the neuron's of the brainstem
  ***************************************************************************/
  void Update();


  /***************************************************************************
  *  @brief: Getters for the left and right output variables
  ***************************************************************************/
  double OutputLeft(){ return d_outputLeft; }
  double OutputRight(){ return d_outputRight; }
};

#endif
