#ifndef BASALGANGLIA_H
#define BASALGANGLIA_H

#include "functionalGroup.hpp"
#include "visuomotorCenter.hpp"

// Forward declaration of the visumotor center class.
class VisuomotorCenter;


class BasalGanglia : public FunctionalGroup {


public:


  /***************************************************************************
  *  @brief: Default constructor
  *
  *  @details: Method to create all neurons of the basal ganglia by assigning
  *            them unique names based on their properties.
  *
  *            Registers the connections between the different neurons of the
  *            basal ganglia.
  ***************************************************************************/
  BasalGanglia();


  /***************************************************************************
  *  @brief: Expose the CreateConnections() method inherited from the base class
  ***************************************************************************/
  using FunctionalGroup::CreateConnections;


  /***************************************************************************
  *  @brief: Connects the basal ganglia to the visuomotor center
  *
  *  @details: Overloads the CreateConnections() method defined in the
  *            FunctionalGroup base class.
  *
  *  @params[in]: VisuomotorCenter*
  *                - A pointer to the robot's visuomotor center object
  ***************************************************************************/
  void CreateConnections(VisuomotorCenter* VMC);


  /***************************************************************************
  *  @brief: Updates the neuron's of the basal ganglia
  *
  *  @details: This method updates the states of the neurons in a rigid order:
  *             1 - STN neurons
  *             2 - GPe neurons
  *             3 - GPi neurons
  ***************************************************************************/
  void Update();
};

#endif
