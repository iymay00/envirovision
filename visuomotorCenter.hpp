#ifndef VISUOMOTORCENTER_H
#define VISUOMOTORCENTER_H

#include "functionalGroup.hpp"
#include "brainstem.hpp"
#include "basalGanglia.hpp"
#include "envirobot.hpp"
#include <map>
#include <tuple>

// Synaptic strength Scalings for the different behaviors
#define PREDATOR_SCALE 1.0
#define OBSTACLE_SCALE 0.5
#define PREY_SCALE 0.7

// Forward declaration of the basal ganglia, brainstem, and Envirobot classes.
class BasalGanglia;
class Envirobot;
class Brainstem;


class VisuomotorCenter : public FunctionalGroup {


public:


  /***************************************************************************
  *  @brief: Default constructor
  *
  *  @details: Method to create all neurons of the visuomotor center by assigning
  *            them unique names based on their properties.
  *
  *            Registers the connections between the different neurons of the
  *            visuomotor center.
  ***************************************************************************/
  VisuomotorCenter();


  /***************************************************************************
  *  @brief: Expose the CreateConnections() method inherited from the base class
  ***************************************************************************/
  using FunctionalGroup::CreateConnections;


  /***************************************************************************
  *  @brief: Connects the visuomotor center to the basal ganglia
  *
  *  @details: Overloads the CreateConnections() method defined in the
  *            FunctionalGroup base class.
  *
  *  @params[in]: BasalGanglia*
  *                 - A pointer to the robot's basal ganglia object
  ***************************************************************************/
  void CreateConnections(BasalGanglia* BG);


  /***************************************************************************
  *  @brief: Connects the visuomotor center to the brainstem
  *
  *  @details: Overloads the CreateConnections() method defined in the
  *            FunctionalGroup base class.
  *
  *  @params[in]: Brainstem*
  *                 - A pointer to the robot's brainstem object
  ***************************************************************************/
  void CreateConnections(Brainstem* B);


  /***************************************************************************
  *  @brief: Updates the neuron's of the visuomotor center
  *
  *  @details: This method updates the states of the neuron layer specified
  *
  *  @params[in]: std::string
  *                - The layer to be updated ("Input"/"Response"/"Auxiliary")
  ***************************************************************************/
  void Update(std::string layer);


  /***************************************************************************
  *  @brief: Transmits the signals in the response layer to the basal ganglia
  *          for arbitration
  ***************************************************************************/
  void Arbitrate();
};

#endif
